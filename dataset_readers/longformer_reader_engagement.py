import os

import torch

os.environ["TOKENIZERS_PARALLELISM"] = "false"

import json
import logging
from typing import Dict, Union

from allennlp.common.file_utils import cached_path
from allennlp.data import Token
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import Field, LabelField, MetadataField, TextField, TensorField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers.pretrained_transformer_indexer import PretrainedTransformerIndexer
from overrides import overrides
# from pytorch_pretrained_bert import BertTokenizer
from transformers import AutoTokenizer

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@DatasetReader.register("engagement-reader")
class HierarchicalBertReader2(DatasetReader):
    def __init__(self,
                 model_name: str = '',
                 skip_label_indexing: bool = True,  # Since I am using numerical labels
                 ) -> None:
        super().__init__()
        self._skip_label_indexing = skip_label_indexing
        # self._token_indexer = {"tokens": PretrainedTransformerIndexer(model_name)}
        self._tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.manual_distributed_sharding = True
        self.manual_multiprocess_sharding = True
    @overrides
    def _read(self, file_path):
        rank, world_size = None, None
        file_path = cached_path(file_path)
        logger.info("Reading file at %s", file_path)
        # distributed = torch.distributed.is_initialized()
        # if distributed:
        #     rank = torch.distributed.get_rank()
        #     world_size = torch.distributed.get_world_size()
        # for idx, line in enumerate(file_path):
        #     if distributed and idx % world_size != rank:
        #         continue
        for line in self.shard_iterable(open(file_path)):
            data = json.loads(line)
            sentence1 = data['text1']  # ctx
            sentence2 = data['text2']  # utt
            label = data['label_engagement']
            sid = data['cid']
            # target_sequence_length = 512

            instance = self.text_to_instance(question_text=sentence2,
                                             paragraph_text=sentence1,
                                             label=label,
                                             sid=sid)
            if instance is not None:
                yield instance

    def _truncate(self, tokens):
        """
        truncate a sentence using the provided sequence length
        """
        if len(tokens) > self._max_sequence_length:
            tokens = tokens[:self._max_sequence_length]
        return tokens

    @overrides
    def text_to_instance(self,
                         question_text: str,
                         paragraph_text: str,
                         label: Union[str, int, float] = None,
                         sid: Union[str, int, float] = None) -> Instance:

        encoding = self._tokenizer.encode_plus(question_text, paragraph_text, add_special_tokens=True, return_special_tokens_mask=True)
        # print('\n' + self._tokenizer.decode(encoding["input_ids"]) + '\n')

        input_ids = encoding["input_ids"]
        attention_mask = encoding["attention_mask"]

        # Global attention to <s>, </s> tokens
        # global_attention_mask = [1 if token in self._tokenizer.all_special_ids else 0 for token in input_ids]

        global_attention_mask = torch.zeros_like(torch.tensor(input_ids)).unsqueeze(0)
        # global attention on cls token
        global_attention_mask[:, 0] = 1

        fields: Dict[str, Field] = {
                                    "sid": MetadataField(sid),
                                    "tokenizer_size": MetadataField(len(self._tokenizer)),
                                    "utterance_raw": MetadataField(question_text),
                                    "context_raw": MetadataField(paragraph_text),
                                    "input_ids": TensorField(torch.tensor(input_ids)),
                                    "attention_mask": TensorField(torch.tensor(attention_mask)),
                                    "global_attention_mask": TensorField(global_attention_mask.clone().detach())}
        if label is not None:
            fields['label'] = LabelField(label, label_namespace="eng_labels",
                                             skip_indexing=self._skip_label_indexing)
        return Instance(fields)
