local model_name = "mrm8488/longformer-base-4096-finetuned-squadv2"; #"allenai/longformer-base-4096";
local effective_batch_size = 16;
local gpu_batch_size = 1;
local num_gpus = 1;
local construct_vocab = false;

local reader_common = {
      "type": "pretrained_transformer",
      "model_name": model_name
};

{
  "dataset_reader": {
    "type": "multitask",
    "readers": {
      "engagement": reader_common {
        "type": "engagement-reader",
        "skip_label_indexing": false
      },
      "topical": reader_common {
        "type": "topical-reader",
        "skip_label_indexing": true
      },
    }
  },
  "vocabulary": {
        "max_vocab_size": 15000
    },
  "train_data_path": {
//    "engagement": "/scratch/ip9/DATA/engagement_train.json",
//    "topical": "/scratch/ip9/DATA/topical_train.json",
    "engagement": "/scratch/ip9/DATA/sample.json",
    "topical": "/scratch/ip9/DATA/sample.json",
  },
  "validation_data_path": {
//    "engagement": "/scratch/ip9/DATA/engagement_validation.json",
//    "topical": "/scratch/ip9/DATA/topical_validation.json",
    "engagement": "/scratch/ip9/DATA/sample.json",
    "topical": "/scratch/ip9/DATA/sample.json",
  },
  "model": {
    "type": "multitask",
    "backbone": {
      "type": "longformer-backbone", #"pretrained_transformer",
      "model_name": model_name,
    },
    "allowed_arguments": {
      'backbone': ["tokenizer_size", "utterance_raw", "context_raw", "input_ids", "attention_mask", "global_attention_mask"],
      'engagement': ['label', 'encoded_text'],
      'topical': ['label', 'encoded_text']
    },
    "heads": {
      "engagement": {
        "type": "classifier-head",
        "dropout": 0.0,
        "num_labels": 5,
        "label_namespace": "eng_labels",
        "class_weights":
            {
            "Excellent":0.0056,
            "Good":0.0062,
            "Passable":0.5,
            "Not Good":0.41,
            "Poor":1
            }
        ,
        "feedforward": {
            "input_dim": 768,
            "num_layers": 5,
            "hidden_dims": [768, 384, 192, 96, 5],
            "activations": ["relu", "relu", "relu", "relu", "relu"],
            "dropout": [0.2, 0.2, 0.2, 0.2, 0.2]
        },
      },
      "topical": {
        "type": "classifier-head",
        "dropout": 0.0,
        "num_labels": 2,
        "label_namespace": "top_labels",
        "class_weights": {"1":0.36, "0":1},
        "feedforward": {
            "input_dim": 768,
            "num_layers": 5,
            "hidden_dims": [768, 384, 192, 96, 5],
            "activations": ["relu", "relu", "relu", "relu", "relu"],
            "dropout": [0.2, 0.2, 0.2, 0.2, 0.2]
        },
      },
    }
  },
  "data_loader": {
    "type": "multitask",
    "scheduler": {
        "batch_size": gpu_batch_size,
    },
    "shuffle": false,
  },
  "trainer": {
    "optimizer": {
      "type": "huggingface_adamw",
      "lr": 4e-5,
      "correct_bias": true,
      "weight_decay": 0.01,
    },
    "learning_rate_scheduler": {
      "type": "linear_with_warmup",
      "warmup_steps": 1500,
    },
    "validation_metric": ["+topical_f1", "+engagement_f1"],
    "patience": 10,
    "num_epochs": 10,
    "callbacks": [{"type": 'tensorboard',},],
//    "grad_scaling": false,
//    "cuda_device": 3,

    "num_gradient_accumulation_steps": effective_batch_size / gpu_batch_size / std.max(1, num_gpus),
//    "use_amp": true
  },
  "distributed": {
    "cuda_devices": [2,3],
    "ddp_accelerator": {
        "type": "torch",
        "find_unused_parameters": true
    }
  }
}
