from typing import List, Dict

import torch
from allennlp.data import Vocabulary
from allennlp.modules import Backbone
from allennlp.modules.backbones import PretrainedTransformerBackbone
from transformers import LongformerModel, AutoTokenizer, AutoModel


@Backbone.register("longformer-backbone")
class LongformerBackbone(PretrainedTransformerBackbone):
    def __init__(self, vocab: Vocabulary, model_name: str):
        super().__init__(vocab, model_name)

        if isinstance(model_name, str):
            self._model = AutoModel.from_pretrained(model_name, output_attentions=True)
            self._tokenizer = AutoTokenizer.from_pretrained(model_name)
        else:
            self._model = model_name
        torch.autograd.set_detect_anomaly(True)

    def forward(self,  # type: ignore
                utterance_raw: List,
                context_raw: List,
                input_ids: torch.LongTensor,
                attention_mask: torch.LongTensor,
                global_attention_mask: torch.LongTensor,
                tokenizer_size: List,
                ) -> Dict[str, torch.Tensor]:

        # text_inputs = next(iter(text.values()))
        # mask = util.get_text_field_mask(text)
        # encoded_text = self._embedder(**text_inputs)
        # outputs = {"encoded_text": encoded_text, "encoded_text_mask": mask}

        # Resize model to correct length
        self._model.resize_token_embeddings(tokenizer_size[0])

        model_output = self._model(input_ids=input_ids,
                              attention_mask=attention_mask,
                              global_attention_mask=global_attention_mask
                              )

        pooled_outputs = model_output[1]
        # return {"encoded_text": pooled_outputs, "encoded_text_mask":pooled_outputs[2]}
        # mask = util.get_text_field_mask(input_ids.tolist())
        # encoded_text = self._embedder(**text_inputs)
        outputs = {"encoded_text": pooled_outputs}
        return outputs
