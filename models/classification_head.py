from typing import Dict, Optional, List, Union

from overrides import overrides
import torch

from allennlp.data import Vocabulary
from allennlp.models.heads.head import Head
from allennlp.modules import FeedForward
from allennlp.training.metrics import Average, CategoricalAccuracy, FBetaMeasure


@Head.register("classifier-head")
class ClassifierHead(Head):
    """
    A classification `Head`.  Takes encoded text, gets a single vector out of it, runs an optional
    feedforward layer on that vector, then classifies it into some label space.

    Registered as a `Head` with name "classifier".

    # Parameters

    vocab : `Vocabulary`
        Used to get the number of labels, if `num_labels` is not provided, and to translate label
        indices to strings in `make_output_human_readable`.
    seq2vec_encoder : `Seq2VecEncoder`
        The input to this module is assumed to be a sequence of encoded vectors.  We use a
        `Seq2VecEncoder` to compress this into a single vector on which we can perform
        classification.
    feedforward : `FeedForward`, optional, (default = `None`)
        An optional feedforward layer to apply on the pooled output before performing the
        classification.
    input_dim : `int`, optional (default = `None`)
        We need to know how many dimensions to use for the final classification weight matrix.  If
        you have provided either a `seq2vec_encoder` or a `feedforward` module, we can get the
        correct size from those objects.  If you use default values for both of those parameters,
        then you must provide this parameter, so that we know the size of that encoding.
    dropout : `float`, optional (default = `None`)
        Dropout percentage to use.
    num_labels : `int`, optional (default = `None`)
        Number of labels to project to in classification layer. By default, the classification layer will
        project to the size of the vocabulary namespace corresponding to labels.
    label_namespace : `str`, optional (default = `"labels"`)
        Vocabulary namespace corresponding to labels. By default, we use the "labels" namespace.
    """

    def __init__(
        self,
        vocab: Vocabulary,
        feedforward: Optional[FeedForward] = None,
        input_dim: int = None,
        dropout: float = None,
        num_labels: int = None,
        class_weights: Optional[Dict[Union[str, int], float]] = {},
        label_namespace: str = "labels",
    ) -> None:

        super().__init__(vocab)
        self._feedforward = feedforward
        if self._feedforward is not None:
            self._classifier_input_dim = self._feedforward.get_output_dim()
        else:
            self._classifier_input_dim = input_dim

        if self._classifier_input_dim is None:
            raise ValueError("No input dimension given!")

        if dropout:
            self._dropout = torch.nn.Dropout(dropout)
        else:
            self._dropout = None
        self._label_namespace = label_namespace

        if num_labels:
            self._num_labels = num_labels
        else:
            self._num_labels = vocab.get_vocab_size(namespace=self._label_namespace)
        self._classification_layer = torch.nn.Linear(self._classifier_input_dim, self._num_labels)
        self._class_weights = self.get_class_weights(class_weights=class_weights, label_namespace=label_namespace)
        self._accuracy = CategoricalAccuracy()
        self._f1 = FBetaMeasure(average="weighted")
        if self._class_weights:
            self._loss = torch.nn.CrossEntropyLoss(weight=torch.tensor(self._class_weights))
        else:
            self._loss = torch.nn.CrossEntropyLoss()


    def forward(  # type: ignore
        self,
        encoded_text: torch.FloatTensor,
        label: torch.IntTensor = None,
    ) -> Dict[str, torch.Tensor]:
        # if torch.isfinite(encoded_text).any() or torch.isfinite(label).any():
        #     torch.cuda.amp.autocast(enabled=False)
        #     print(encoded_text, label)
        if self._dropout:
            encoded_text = self._dropout(encoded_text)

        if self._feedforward is not None:
            encoded_text = self._feedforward(encoded_text)

        logits = self._classification_layer(encoded_text)
        probs = torch.nn.functional.softmax(logits, dim=-1)

        output_dict = {"logits": logits, "probs": probs}
        if label is not None:
            loss = self._loss(logits, label.long().view(-1))
            output_dict["loss"] = loss
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict

    @overrides
    def make_output_human_readable(
        self, output_dict: Dict[str, torch.Tensor]
    ) -> Dict[str, torch.Tensor]:
        """
        Does a simple argmax over the probabilities, converts index to string label, and
        add `"label"` key to the dictionary with the result.
        """
        if "probs" in output_dict:
            predictions = output_dict["probs"]
            if predictions.dim() == 2:
                predictions_list = [predictions[i] for i in range(predictions.shape[0])]
            else:
                predictions_list = [predictions]
            classes = []
            for prediction in predictions_list:
                label_idx = prediction.argmax(dim=-1).item()
                label_str = self.vocab.get_index_to_token_vocabulary(self._label_namespace).get(
                    label_idx, str(label_idx)
                )
                classes.append(label_str)
            output_dict["label"] = classes
        return output_dict

    def get_class_weights(self, class_weights, label_namespace="labels"):
        weights: List[float] = [0.0] * len(class_weights)
        for label, weight in class_weights.items():
            try:
                label = int(label)
                label_idx = label
            except ValueError:
                label_idx = self.vocab.get_token_index(label, namespace=label_namespace)
            weights[label_idx] = weight
        return weights

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        precision, recall, f1 = self._f1.get_metric(reset).values()
        metrics = {"accuracy": self._accuracy.get_metric(reset),
                   "precision": precision,
                   "recall": recall,
                   "f1": f1}

        return metrics
